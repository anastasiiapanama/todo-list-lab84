const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    status: {
        type: String,
        enum: ['New', 'In_progress', 'Complete']
    }
});

const TaskUser = mongoose.model('TaskSchema', TaskSchema);

module.exports = TaskUser;