const express = require('express');
const Task = require('../models/TaskUser');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {
        const task = req.body;

        const newTask = new Task(task);
        newTask.user = req.body.user;

        await newTask.save();

        return res.send(newTask);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const criteria = {};

        if(req.query.user) {
            criteria.user = req.query.user;
        }

        const taskList = await Task.find(criteria).populate('user', 'username token');

        return res.send(taskList);
    } catch (e) {

    }
});

router.put('/:id', auth, async (req, res) => {
    try {
        const tasksRequestData = req.body;

        const result = await Task.findById({_id: req.params.id}).update(tasksRequestData);

        res.send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOne({_id: req.params.id}).remove();

        res.send(task);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;